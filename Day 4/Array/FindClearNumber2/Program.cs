﻿namespace FindClearNumber2 // Note: actual namespace depends on the project name.
{
    public class Program
    {
        static void Main(string[] args)
        {
            int[] arr = new int[] { 2, 3, };

            double findNumb = FindMissingNumber(arr);
            Console.WriteLine(findNumb);
        }

        public static double FindMissingNumber(int[] arr)
        {
            // Check Array length
            if (num.Length == 0)
            {
                throw new ArgumentException("Array is empty");
            }

            double minNumber = arr[0];
            double maxNumber = 0;
            double arrLength = arr.Length + 1;
            double sumArray = 0;

            for (int i = 0; i < arr.Length; i++)
            {
                if (maxNumber < arr[i]) 
                {
                    maxNumber = arr[i];
                }

                if (minNumber > arr[i]) 
                { 
                    minNumber = arr[i]; 
                }

                sumArray += arr[i];
            }

            double number = ((minNumber + maxNumber) / 2) * arrLength;

            double findNumber = number - sumArray;

            return findNumber;
        }
    }
}