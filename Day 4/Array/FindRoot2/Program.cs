﻿namespace FindRoot2 // Note: actual namespace depends on the project name.
{
    public class Root
    {
        static void Main(string[] args)
        {

            int input = int.Parse(Console.ReadLine());

            double root = FindRoot(input);

            Console.WriteLine(root);
        }

        public static int FindRoot(double input)
        {

            int result = 0;
            // Must be optimised
            for (int i = 0; i < 100; i++)
            {
                int root = i * i * i;

                if (root == input)
                {
                    return i;
                }
            }

            return result;
        }
    }
}