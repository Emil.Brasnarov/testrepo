﻿using System;
using System.Collections.Immutable;

namespace FindClearNumber // Note: actual namespace depends on the project name.
{
    public class Program
    {
        static void Main(string[] args)
        {

            //int[] arr = new int[] { 1, 3, 2, 5, 6 };
            //int[] num = SortedArr(arr);

            //int findNumb = FindMissingNumber(arr);
            //Console.WriteLine(findNumb);
        }

        public static int FindMissingNumber(int[] num)
        {
            // Check Array length
            if (num.Length == 0)
            {
                throw new ArgumentException("Array is empty");
            }

            // Sorted Array
            int[] numSorted = SortedArr(num);

            int findNum = 0;
            // Find missing number from the row
            for (int i = 0; i <= numSorted.Length - 1; i++)
            {
                if (numSorted[i] != i + 1)
                {
                    findNum = numSorted[i] - 1;
                    break;
                }
            }

            return findNum;
        }

        static int[] SortedArr(int[] arr)
        {
            // Sort by ascending 
            int temp = 0;
            for (int j = 0; j <= arr.Length - 1; j++)
            {

                for (int i = 0; i <= arr.Length - 2; i++)
                {
                    if (arr[i] > arr[i + 1])
                    {
                        temp = arr[i + 1];
                        arr[i + 1] = arr[i];
                        arr[i] = temp;
                    }
                }
            }

            return arr;
        }
    }
}